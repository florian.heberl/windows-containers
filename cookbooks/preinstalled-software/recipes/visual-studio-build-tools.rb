require_relative '../libraries/chocolatey'

# https://chocolatey.org/packages/visualstudio2019buildtools
chocolatey_package 'Install Visual Studio 2019 Build Tools' do
  package_name 'visualstudio2019buildtools'
  version node[:visual_studio_build_tool][:version]
  action :install
  # 3010 is returned by chocolatey for dotnetfx because it
  # requires a reboot to be fully installed. The reboot is handled
  # elsewhere.
  returns [0, 3010]
end

# https://chocolatey.org/packages/visualstudio2019-workload-manageddesktopbuildtools
chocolatey_package 'Install Visual Studio 2019 Build Tools .Net desktop workload' do
  package_name 'visualstudio2019-workload-manageddesktopbuildtools'
  version node[:visual_studio_build_tool][:desktop_workload_version]
  action :install
end

# https://chocolatey.org/packages/visualstudio2019-workload-netcorebuildtools
chocolatey_package 'Install Visual Studio 2019 Build Tools .NET core workload' do
  package_name 'visualstudio2019-workload-netcorebuildtools'
  version node[:visual_studio_build_tool][:dotnetcore_workload_version]
  action :install
end

# https://chocolatey.org/packages/visualstudio2019-workload-vctools
chocolatey_package 'Install Visual Studio 2019 Build Tools C++ workload' do
  package_name 'visualstudio2019-workload-vctools'
  version node[:visual_studio_build_tool][:cpp_workload_version]
  action :install
end

# https://chocolatey.org/packages/visualstudio2019-workload-webbuildtools
chocolatey_package 'Install Visual Studio 2019 Build Tools Web development workload' do
  package_name 'visualstudio2019-workload-webbuildtools'
  version node[:visual_studio_build_tool][:web_workload_version]
  action :install
  # 3010 is returned by chocolatey for this build tool because it
  # requires a reboot to be fully installed. The reboot is handled
  # elsewhere.
  returns [0, 3010]
end

# https://chocolatey.org/packages/visualstudio2019-workload-xamarinbuildtools
chocolatey_package 'Install Visual Studio 2019 Build Tools Mobile development workload' do
  package_name 'visualstudio2019-workload-xamarinbuildtools'
  version node[:visual_studio_build_tool][:mobile_workload_version]
  action :install
  # 3010 is returned by chocolatey for this build tool because it
  # requires a reboot to be fully installed. The reboot is handled
  # elsewhere.
  returns [0, 3010]
end

