docker_version = node[:docker][:docker_version]
nuget_package_provider_version = node[:docker][:nuget_package_provider_version]

install_ps1 = File.join(Chef::Config['file_cache_path'], 'install-docker-ce.ps1')
cache_docker_images_ps1 = File.join(Chef::Config['file_cache_path'], 'cache-docker-images.ps1')
ensure_docker_daemon_json_ps1 = File.join(Chef::Config['file_cache_path'], 'ensure-docker-daemon-json.ps1')

cookbook_file install_ps1 do
  action :create
  backup false
  source 'install-docker-ce.ps1'
end

cookbook_file ensure_docker_daemon_json_ps1 do
  action :create
  backup false
  source 'ensure-docker-daemon-json.ps1'
end

cookbook_file cache_docker_images_ps1 do
  action :create
  backup false
  source 'cache-docker-images.ps1'
end

powershell_script 'Install Docker' do
  cwd Chef::Config['file_cache_path']
  code "#{install_ps1} -DockerVersion #{docker_version}"
  live_stream true
end

powershell_script 'Ensure daemon.json file' do
  cwd Chef::Config['file_cache_path']
  code ensure_docker_daemon_json_ps1
  live_stream true
end

powershell_script 'Cache docker images' do
  cwd Chef::Config['file_cache_path']
  code cache_docker_images_ps1
  live_stream true
end
