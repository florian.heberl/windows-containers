$tag = @{
    # build numbers for Windows Server can be found here:
    # https://en.wikipedia.org/wiki/List_of_Microsoft_Windows_versions#Server_versions
    '17763' = 'ltsc2019'
    '20348' = 'ltsc2022'
}[(Get-Item "HKLM:SOFTWARE\Microsoft\Windows NT\CurrentVersion").GetValue('CurrentBuild')]

# ensure docker has started before pulling images
start-service docker

docker pull mcr.microsoft.com/windows/servercore:$tag
docker pull mcr.microsoft.com/windows/nanoserver:$tag
